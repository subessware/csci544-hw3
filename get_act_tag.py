'''
def getActTags():
	inputFile = open("data/baseline_25.data", 'r')  
	output = open("data/baseline_actual.data", 'w+')  
	for line in inputFile:
		if type(line) is not None and len(line) > 0:
			convo = line.rstrip().split("\t")[0]
			output.write(convo + "\n")
	output.close()
	inputFile.close()

if __name__ == '__main__':
	getActTags()'''

import sys
import csv 
def getActTags():
	inputFile = open(sys.argv[1], 'r')  
	tag = open(sys.argv[2], 'w+')  
	unk = open(sys.argv[3], 'w+')
	for line in inputFile:
		if type(line) is not None and len(line.rstrip()) > 0:
			da = line.rstrip().split("\t")[0]
			convo = line.rstrip().split("\t")[1:]
			
			tag.write(da + "\n")
			writeStr = "UNK" + "\t"
			for w in convo:
				#print(w)
				writeStr += w + "\t"
			#print(writeStr)
			unk.write(writeStr+"\n")
		else:
			tag.write("\n")
			unk.write("\n");
	tag.close()
	unk.close()
	inputFile.close()

if __name__ == '__main__':
	getActTags()
