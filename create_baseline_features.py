import json
import os
from hw3_corpus_tool import *
import sys

def createFeatureFile():
	files = []
	'''for filename in os.listdir("data/train/"):
		if filename == ".DS_Store":
           		continue
		file_path = "data/train/"+filename
		files.append(file_path)
	files = sorted(files)'''
	#inputFile = open(sys.argv[1], 'r')  
	#for filename in files:
	convo = get_utterances_from_filename(sys.argv[1])
	first = True
	speaker_change = True
	featureRep = ""	
	curr_speaker = ""
	prev_speaker = ""	
	for utterance in convo:
		curr_speaker = utterance[1]
		speaker_change = True
		if first or curr_speaker == prev_speaker:
			speaker_change = False	
		featureRep = utterance[0] + "\t" #+ utterance[1] + "\t"
		if utterance[2] is not None:
			for feature in utterance[2]:
				featureRep+= "TOKEN_"+feature[0] + "\t" + "POS_"+feature[1] +"\t"
		if first:
			featureRep+= "FIRST" + "\t"
			first = False
		if speaker_change:
			featureRep += "SPEAKER_CHANGED" 
		prev_speaker = curr_speaker			
		print(featureRep)
		#output.write("\n")
	#inputFile.close()

if __name__ == '__main__':
	createFeatureFile()
