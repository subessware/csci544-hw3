import json
import os
from hw3_corpus_tool import *
import sys

def createFeatureFile():
	files = []
	for filename in os.listdir(sys.argv[1]):
		if filename == ".DS_Store":
           		continue
		file_path = sys.argv[1]+ "/"+filename
		files.append(file_path)
	#files = sorted(files)
	i = 1
	train = len(files) * 0.75
	#print (train)
	output = open(sys.argv[2], 'w+')  
	test = open(sys.argv[3], 'w+')
	for filename in files:
		convo = get_utterances_from_filename(filename)
		first = True
		speaker_change = True
		featureRep = ""	
		curr_speaker = ""
		prev_speaker = ""
		if i <= train:
			for utterance in convo:
				prev_utterance = ""
				prev_pos = ""
				prev_prev_pos = ""
				prev_prev_utter = ""
				if utterance[0] is None:
					featureRep = "UNK" + "\t" + utterance[1] + "\t"
				else:
					featureRep = utterance[0] + "\t" + utterance[1] + "\t"
				curr_speaker = utterance[1]
				speaker_change = True
				if first or curr_speaker == prev_speaker:
					speaker_change = False
				if utterance[2] is not None:	
					for feature in utterance[2]:
						featureRep+= "TOKEN_"+feature[0] + "\t" + "POS_"+feature[1] +"\t"
						if len(prev_utterance) > 0:
							featureRep += "TOKEN_"+ prev_utterance + "|TOKEN_"+feature[0] + "\t" + "POS_"+prev_pos + "|POS_"+feature[1] +"\t"
						#if len(prev_prev_pos) > 0:
						#	featureRep += "POS_"+prev_prev_pos +"|POS_"+prev_pos + "|POS_"+feature[1] +"\t"+"TOKEN_"+prev_prev_utter +"|TOKEN_"+prev_utterance + "|TOKEN_"+feature[0] + "\t"
						prev_prev_pos = prev_pos
						prev_prev_utter = prev_utterance
						prev_utterance = feature[0] 
						prev_pos = feature[1]
				if first:
					featureRep+= "FIRST" + "\t"
					first = False
				if speaker_change:
					featureRep += "SPEAKER_CHANGED" 
				prev_speaker = curr_speaker
				output.write(featureRep + "\n")
			output.write("\n")
			i+=1
		else :
			for utterance in convo:
				prev_utterance = ""
				prev_pos = ""
				prev_prev_utter = ""
				prev_prev_pos = ""
				if utterance[0] is None:
					featureRep = "UNK" + "\t" + utterance[1] + "\t"
				else:
					featureRep = utterance[0] + "\t" + utterance[1] + "\t"
				curr_speaker = utterance[1]
				speaker_change = True
				if first or curr_speaker == prev_speaker:
					speaker_change = False
				if utterance[2] is not None:	
					for feature in utterance[2]:
						featureRep+= "TOKEN_"+feature[0] + "\t" + "POS_"+feature[1] +"\t"
						if len(prev_utterance) > 0:
							featureRep += "TOKEN_"+ prev_utterance + "|TOKEN_"+feature[0] + "\t" + "POS_"+prev_pos + "|POS_"+feature[1] +"\t"
						#if len(prev_prev_pos) > 0:
						#	featureRep += "POS_"+prev_prev_pos +"|POS_"+prev_pos + "|POS_"+feature[1] +"\t"+"TOKEN_"+prev_prev_utter +"|TOKEN_"+prev_utterance + "|TOKEN_"+feature[0] + "\t"
						prev_prev_pos = prev_pos
						prev_prev_utter = prev_utterance
						prev_utterance = feature[0] 
						prev_pos = feature[1]
				if first:
					featureRep+= "FIRST" + "\t"
					first = False
				if speaker_change:
					featureRep += "SPEAKER_CHANGED" 
				prev_speaker = curr_speaker
				test.write(featureRep + "\n")
			test.write("\n")
			i+=1
	output.close()
	test.close()

if __name__ == '__main__':
	createFeatureFile()
