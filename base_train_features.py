import json
import os
from hw3_corpus_tool import *
import sys

def createFeatureFile():
	files = []
	for filename in os.listdir(sys.argv[1]):
		if filename == ".DS_Store":
           		continue
		file_path = sys.argv[1]+ "/"+filename
		files.append(file_path)
	#files = sorted(files)
	i = 1
	train = len(files) * 0.75
	print (train)
	output = open(sys.argv[2], 'w+')  
	test = open(sys.argv[3], 'w+')
	for filename in files:
		
		if i <= train:
			convo = get_utterances_from_filename(filename)
			first = True
			speaker_change = True
			featureRep = ""	
			curr_speaker = ""
			prev_speaker = ""
			for utterance in convo:
			
				if utterance[0] is None:
					featureRep = "UNK" + "\t"
				else:
					featureRep = utterance[0] + "\t"
				curr_speaker = utterance[1]
				speaker_change = True
				if first or curr_speaker == prev_speaker:
					speaker_change = False	
				if utterance[2] is not None:
					for feature in utterance[2]:
						featureRep+= "TOKEN_"+feature[0] + "\t" + "POS_"+feature[1] +"\t"
				if first:
					featureRep+= "FIRST" + "\t"
					first = False
				if speaker_change:
					featureRep += "SPEAKER_CHANGED" 
				prev_speaker = curr_speaker
				output.write(featureRep + "\n")
			output.write("\n")
			i+=1
		else:
			convo = get_utterances_from_filename(filename)
			first = True
			speaker_change = True
			featureRep = ""	
			curr_speaker = ""
			prev_speaker = ""
			for utterance in convo:
				if utterance[0] is None:
					featureRep = "UNK" + "\t"
				else:
					featureRep = utterance[0] + "\t"
				curr_speaker = utterance[1]
				speaker_change = True
				if first or curr_speaker == prev_speaker:
					speaker_change = False	
				if utterance[2] is not None:
					for feature in utterance[2]:
						featureRep+= "TOKEN_"+feature[0] + "\t" + "POS_"+feature[1] +"\t"
				if first:
					featureRep+= "FIRST" + "\t"
					first = False
				if speaker_change:
					featureRep += "SPEAKER_CHANGED" 
				prev_speaker = curr_speaker
				test.write(featureRep + "\n")
			test.write("\n")
			i+=1
	output.close()
	test.close()

if __name__ == '__main__':
	createFeatureFile()
